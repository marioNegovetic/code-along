import schemaScalars from "./schemaScalars";
import todoSchema from "./todoSchema";


const typeDefs = [schemaScalars, todoSchema];

export default typeDefs;