import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";


@Entity({name: "todo"})
export class TodoEntity extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: String;

    @Column({nullable: true})
    content?: String;

    @Column({default: false})
    completed: Boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}