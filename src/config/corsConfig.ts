import { CorsOptions } from "cors"

const whitelist: string[] = [
    "https://localhost:"+process.env.PORT,
    "https://localhost:"+process.env.PORT+"/",
    "https://studio.apollographql.com"
]

const corsConfig: CorsOptions  = {
    origin: function (origin, callback) {
      if (typeof origin === "undefined" || whitelist.includes(origin) ) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    }
  }

  export default  corsConfig;