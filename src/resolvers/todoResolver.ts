import { getRepository } from "typeorm";
import { TodoEntity } from "../database/entities/TodoEntity";
import { ICreateTodoInput, IGetTodoInput, IUpdateTodoInput } from "../schema/todoSchema";


const todoResolvers = {
    Query: {
        getTodos: async() => {
            const allTodos = await getRepository(TodoEntity).find();

            return allTodos;
        },
        getTodo: async( _: unknown, args: IGetTodoInput) => {
            const todo = await getRepository(TodoEntity).findOneOrFail({
                where: {
                   id: args.filter.id
                }
            });

           return todo;
        },
    },
    Mutation: {
        createTodo: async ( _:unknown, args: ICreateTodoInput) => {

            const newTodo = new TodoEntity();

            newTodo.title = args.input.title;
            newTodo.content = args.input.content;

            const savedTodo = await newTodo.save();

            return savedTodo;    
        },

        updateTodo: async (_:unknown, args: IUpdateTodoInput) =>{

            //validacija inputa
            if(!args.input.title && !args.input.content && typeof args.input.completed !== "boolean"){
                throw new Error("you must specific at leat one property to update");
            }
            
            const todo = await getRepository(TodoEntity).findOneOrFail({
                where: {
                    id: args.input.id
                }
            });
            
            if(args.input.title){
                todo.title = args.input.title;
            }

            if(args.input.content){
                todo.content = args.input.content;
            }

            if(typeof args.input.completed === "boolean"){
                todo.completed = args.input.completed;
            }

            const updateTodo = await todo.save();

            return updateTodo;
        }
    }
}

export default todoResolvers;